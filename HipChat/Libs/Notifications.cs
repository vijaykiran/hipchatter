﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace HipChat.Libs
{
    class Notifications
    {
        /// <summary>
        /// Displays a toast notification to the user
        /// </summary>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="param">JID</param>
        public static void DisplayToast(string title, string description = null, string param = null)
        {
            if (param != null && param.Contains("jid://"))
            {
                // Is probably a message to a room. Let's check if the user is not already here
                var frame = (Frame)Window.Current.Content;

                if (frame != null)
                {
                    if (frame.Content != null && frame.Content.GetType() == typeof(PublicRoomPage))
                    {
                        string jid = param.Replace("jid://", "");
                        PublicRoomPage page = frame.Content as PublicRoomPage;

                        if (page.chat != null && page.chat.Jid.Bare == jid) return;
                    }
                }
            }

            XmlDocument template = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText02);
         
            var element = template.GetElementsByTagName("text")[0];
            element.AppendChild(template.CreateTextNode(title));

            if (description != null)
            {
                element = template.GetElementsByTagName("text")[1];
                element.AppendChild(template.CreateTextNode(description));
            }


            if (param != null)
            {
                IXmlNode toastNode = template.SelectSingleNode("/toast");
                ((XmlElement)toastNode).SetAttribute("launch", param);
            }

            var toast = new ToastNotification(template);

            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }
    }
}
