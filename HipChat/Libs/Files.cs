﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;

namespace HipChat.Libs
{
    class Files
    {
        private static string BucketName = "hipchat.windows.phone";
        private static string AWSAccessKey = "AKIAJ7GVISSR34Y3CSIA";
        private static string AWSSecretKey = "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";

        public static void SelectFile(string continuationData)
        {
            var filePicker = new FileOpenPicker();
            filePicker.FileTypeFilter.Add(".jpg");
            //filePicker.FileTypeFilter.Add(".mp4");
            filePicker.FileTypeFilter.Add(".png");
            filePicker.FileTypeFilter.Add(".pdf");
            filePicker.ViewMode = PickerViewMode.Thumbnail;
            filePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            filePicker.SettingsIdentifier = "Attachment";
            filePicker.CommitButtonText = "Attach";
            filePicker.ContinuationData["Operation"] = continuationData;

            filePicker.PickSingleFileAndContinue();
        }

        public static async Task<string> UploadFile(StorageFile file)
        {
            AmazonS3Config config = new AmazonS3Config();
            config.RegionEndpoint = RegionEndpoint.EUWest1;

            var client = Amazon.AWSClientFactory.CreateAmazonS3Client(
                AWSAccessKey, AWSSecretKey, config
            );

            var user = API.Current.CurrentUser.Jid.Replace("@chat.hipchat.com", "");

            TransferUtilityUploadRequest uploadRequest = new TransferUtilityUploadRequest()
            {
                BucketName = BucketName,
                Key = user + "/" + DateTime.Now.ToString("ddMMyyhms") + file.FileType,
                StorageFile = file,
                ServerSideEncryptionMethod = ServerSideEncryptionMethod.AES256,
                CannedACL = S3CannedACL.PublicRead
            };

            var uploader = new TransferUtility(client);
            await uploader.UploadAsync(uploadRequest);

            return String.Format("https://s3-eu-west-1.amazonaws.com/{0}/{1}", BucketName, uploadRequest.Key);
        }
    }
}
