﻿using HipChat.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HipChat.Libs
{
    public class Account
    {
        public Windows.Storage.ApplicationDataContainer settings = Windows.Storage.ApplicationData.Current.LocalSettings;

        private static string AccountLogged = "Account.Logged";
        private static string AccountEmail = "Account.Email";
        private static string AccountPassword = "Account.Password";
        private static string AccountChats = "Account.OpenChats";
        private static string AccountStatus = "Account.OnlinePresenceStatus";
        private static string AccountTutorialSeen = "Account.TutorialSeen";
        private static string AccountPushNotifications = "Account.PushNotifications";
        private static string AccountJumpRecents = "Account.JumpRecents";

        public static Account Current
        {
            get
            {
                return (Windows.UI.Xaml.Application.Current as App).account;
            }
        }

        public bool isLoggedIn
        {
            get
            {
                return settings.Values.ContainsKey(AccountLogged);
            }
        }

        public string Email
        {
            get
            {
                return settings.Values.ContainsKey(AccountEmail) ? settings.Values[AccountEmail].ToString() : null;
            }
        }

        public string Password
        {
            get
            {
                return settings.Values.ContainsKey(AccountPassword) ? settings.Values[AccountPassword].ToString() : null;
            }
        }

        public void Login(string email, string password)
        {
            settings.Values[AccountLogged] = true;
            settings.Values[AccountEmail] = email;
            settings.Values[AccountPassword] = password;

            System.Diagnostics.Debug.WriteLine("Login data saved locally.");
        }

        public void Logout()
        {
            try
            {
                settings.Values.Remove(AccountLogged);
                settings.Values.Remove(AccountPassword);
                settings.Values.Remove(AccountStatus);
                settings.Values.Remove(AccountChats);
            }
            catch (Exception) { }

            ContactsManager.DeleteAllContacts();
        }

        public void SaveOpenChats()
        {
            if (API.Current.Chats.Count > 0)
            {
                settings.Values[AccountChats] = String.Join(";", (from Chat c in API.Current.Chats select c.Jid.Bare).ToArray());
                System.Diagnostics.Debug.WriteLine("Saved open chats: " + settings.Values[AccountChats]);
            }
            else
            {
                settings.Values.Remove(AccountChats);
            }
        }

        /// <summary>
        /// Joins the rooms that should be open
        /// </summary>
        /// <param name="api"></param>
        public void LoadOpenChats(API api)
        {
            if (api.Chats.Count == 0 && settings.Values.ContainsKey(AccountChats))
            {
               var chats = settings.Values[AccountChats].ToString().Split(';');
                foreach (string chatjid in chats)
                {
                    Room room = (from Room r in api.Rooms where r.Jid.Bare == chatjid select r).FirstOrDefault();
                    if (room != null)
                    {
                        API.Current.JoinRoom(room);
                    }
                }
            }
        }

        public string OnlinePresence
        {
            get
            {
                return settings.Values.ContainsKey(AccountStatus) ? settings.Values[AccountStatus].ToString() : null;
            }

            set
            {
                settings.Values[AccountStatus] = value;
            }
        }

        public bool HasOfflinePresence
        {
            get
            {
                return settings.Values.ContainsKey(AccountStatus) ? settings.Values[AccountStatus].ToString() == "offline" : false;
            }
        }

        public bool HasSeenTutorial
        {
            get
            {
                return settings.Values.ContainsKey(AccountTutorialSeen) ? bool.Parse(settings.Values[AccountTutorialSeen].ToString()) : false;
            }
            set
            {
                settings.Values[AccountTutorialSeen] = value;
            }
        }

        public bool HasPushNotificationsEnabled
        {
            get
            {
                return settings.Values.ContainsKey(AccountPushNotifications) ? bool.Parse(settings.Values[AccountPushNotifications].ToString()) : false;
            }
            set
            {
                settings.Values[AccountPushNotifications] = value;
            }
        }

        public bool ShouldJumpToRecents
        {
            get
            {
                return settings.Values.ContainsKey(AccountJumpRecents) ? bool.Parse(settings.Values[AccountJumpRecents].ToString()) : true;
            }
            set
            {
                settings.Values[AccountJumpRecents] = value;
            }
        }
    }
}
