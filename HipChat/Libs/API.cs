﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Matrix;
using Matrix.Xmpp.Client;
using System.Collections.ObjectModel;
using HipChat.Model;
using Windows.UI.Xaml;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Matrix.Xmpp;
using System.Xml.Linq;

namespace HipChat.Libs
{
    public sealed class API
    {
        private static readonly API instance = new API();

        private XmppClient client;
        private MucManager muc;
        private DiscoManager disco;

        private bool _isConnected = false;
        private bool _isConnecting = false;
        private bool _isFetchingRooms = false;

        private Contact _currentUser;

        public ObservableCollection<Contact> Contacts = new ObservableCollection<Contact>();
        public ObservableCollection<Room> Rooms = new ObservableCollection<Room>();
        public ObservableCollection<Chat> Chats = new ObservableCollection<Chat>();

        DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);

        System.Xml.Linq.XNamespace nsRoom = "http://hipchat.com/protocol/muc#room";

        private DispatcherTimer _pingTimer;

        private API()
        {
            SetLicense();

            _pingTimer = new DispatcherTimer();
            _pingTimer.Interval = TimeSpan.FromSeconds(60);
            _pingTimer.Tick += _pingTimer_Tick;

            SetupClientInstance();
        }

        public void SetupClientInstance()
        {
            if (_isConnecting)
            {
                System.Diagnostics.Debug.WriteLine("[API] Cannot create a new XMPP instance. The client is already connecting");
                return;
            }

            if (client != null)
            {
                client.OnLogin -= Client_OnLogin;
                client.OnRosterItem -= Client_OnRosterItem;
                client.OnRosterEnd -= Client_OnRosterEnd;
                client.OnPresence -= Client_OnPresence;
                client.OnMessage -= Client_OnMessage;
                client.OnIq -= client_OnIq;
            }

            System.Diagnostics.Debug.WriteLine("[API] New XMPP Client instance created");

            client = new XmppClient();
            client.Dispatcher = Window.Current.Dispatcher;

            client.Status = "HipChatter for Windows Phone";
            client.Resource = "HipChatter-WP8.1";
            client.XmppDomain = "chat.hipchat.com";
            client.StartTls = true;
            client.AutoReplyToPing = true;

            client.OnLogin += Client_OnLogin;
            client.OnRosterItem += Client_OnRosterItem;
            client.OnRosterEnd += Client_OnRosterEnd;
            client.OnPresence += Client_OnPresence;
            client.OnMessage += Client_OnMessage;
            client.OnIq += client_OnIq;
            client.OnAuthError += client_OnAuthError;
            client.OnError += client_OnError;

#if DEBUG
            client.OnSendXml += client_OnSendXml;
            client.OnReceiveXml += client_OnReceiveXml;
#endif

            if (muc != null)
            {
                muc.OnInvite += muc_OnInvite;
            }

            muc = new MucManager(client);
            muc.OnInvite += muc_OnInvite;

            if (disco != null)
            {
                disco.OnDiscoItemsResult -= disco_OnRoomItemReceived;
            }

            disco = new DiscoManager(client);
            disco.OnDiscoItemsResult += disco_OnRoomItemReceived;
        }

        public static API Current
        {
            get
            {
                return instance;
            }
        }

        public DiscoManager Disco
        {
            get
            {
                return disco;
            }
        }

        public void OpenConnection()
        {
            if (_isConnecting) return;

            LoadAccountPresence();

            _isConnecting = true;
            client.Open();
        }

        private void client_OnError(object sender, ExceptionEventArgs e)
        {
            _isConnecting = false;
        }

        private void client_OnAuthError(object sender, Matrix.Xmpp.Sasl.SaslEventArgs e)
        {
            _isConnecting = false;
        }

        private void Client_OnRosterEnd(object sender, Matrix.EventArgs e)
        {
            //RejoinOpenChats();
            GetRooms();
        }

        private void RejoinOpenChats()
        {
            // Rejoin the open chats
            if (Rooms.Count > 0)
            {
                System.Diagnostics.Debug.WriteLine("Rejoining open chats: " + Chats.Count);
                foreach (Chat chat in Chats)
                {
                    if (chat.IsRoom) JoinRoom(chat.Resource as Room, true);
                }
            }
        }

        void client_OnSendXml(object sender, TextEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(e.Text);
        }

        void client_OnReceiveXml(object sender, TextEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(e.Text);
        }

        public XmppClient Client
        {
            get
            {
                return client;
            }
        }

        /// <summary>
        /// Gets the logged user
        /// </summary>
        public Contact CurrentUser
        {
            get
            {
                if (_currentUser == null)
                {
                    string email = Account.Current.Email;

                    foreach (Contact person in Contacts)
                    {
                        if (person.Jid.Contains(email))
                        {
                            _currentUser = person;
                            break;
                        }
                    }
                }

                return _currentUser;
            }
        }

        private void _pingTimer_Tick(object sender, object e)
        {
            SendKeepAlive();
        }

        public void SendKeepAlive()
        {
            if (!_isConnected) return;

            try
            {
                client.Send(new PingIq { To = client.XmppDomain, Type = Matrix.Xmpp.IqType.get });
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Cannot send ping: " + ex.Message);
            }
        }

        private void Client_OnLogin(object sender, Matrix.EventArgs e)
        {
            _isConnected = true;
            _isConnecting = false;

            System.Diagnostics.Debug.WriteLine("Login succeded");

            CloudSocket.Close();

            _pingTimer.Start();
        }

        public bool IsConnected
        {
            get
            {
                return _isConnected;
            }
        }

        /// <summary>
        /// Reconnectes the client if possible
        /// </summary>
        public void Reconnect(Action<bool> callback = null)
        {
            if (!_isConnected)
            {
                System.Diagnostics.Debug.WriteLine("[API] Reconnect: socket not connected");

                try
                {
                    if (!_isConnecting)
                    {
                        SetupClientInstance();
                    }

                    if (callback != null)
                    {
                        bool used = false;

                        client.OnRosterEnd += delegate
                        {
                            if (!used)
                            {
                                used = true;
                                callback(true);
                            }
                        };
                    }

                    if (client.Username == null && Account.Current.isLoggedIn)
                    {
                        client.Username = Account.Current.Email;
                        client.Password = Account.Current.Password;
                        OpenConnection();
                    }
                    else if (client.Username != null)
                    {
                        OpenConnection();
                    }
                }
                catch (Exception) {
                    if (callback != null) callback(false);
                }
            }
            else
            {
                if (callback != null) callback(true);
            }
        }

        /// <summary>
        /// Closes the client connection if possible
        /// </summary>
        public void CloseConnection()
        {
            if (_isConnected)
            {
                _pingTimer.Stop();

                try
                {
                    System.Diagnostics.Debug.WriteLine("[API] Socket: Closing connection");
                    Client.Close();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("[API] Socket: Could not close the connection: " + ex.Message);
                }
                finally
                {
                    _isConnected = false;
                    CloudSocket.Open();
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("[API] Socket: no need to close");
            }
        }

        public void Logout()
        {
            CloseConnection();
            Contacts.Clear();
            Rooms.Clear();
            Chats.Clear();
        }

        /// <summary>
        /// Triggered when an invitation to join a public room is triggered
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///

        private async void muc_OnInvite(object sender, MessageEventArgs e)
        {
            var msg = new HipChat.Model.ChatMessage
            {
                From = e.Message.XMucUser.FirstElement.Attribute("from").Value,
                Type = e.Message.Type,
                Body = e.Message.XMucUser.FirstElement.Value,
                Stamp = DateTime.Now
            };

            if (msg.IsOneToOne) return;

            Room room = (from Room r in Rooms where r.Jid.Bare == e.Message.From.Bare select r).FirstOrDefault();

            if (room == null) return;

            var dlg = new MessageDialog(
                String.Format("You were invited to join the room {0}.\r\n\r\n{1} mentioned you:\r\n{2}", room.Name, msg.FromName, msg.Body),
                "\r\nInvitation received"
            );

            Notifications.DisplayToast(room.Name, String.Format("{0}: {1}", msg.FromName, msg.Body), "jid://" + room.Jid.Bare);

            dlg.Commands.Add(new UICommand
            {
                Label = "Join",
                Id = 0
            });
            dlg.Commands.Add(new UICommand
            {
                Label = "Ignore",
                Id = 1
            });
            dlg.CancelCommandIndex = 1;

            try
            {
                IUICommand command = await dlg.ShowAsync();

                if (command.Id.ToString() == "0")
                {
                    var frame = (Frame)Window.Current.Content;

                    if (room != null)
                    {
                        JoinRoom(room);
                        frame.Navigate(typeof(PublicRoomPage), room.chat.Jid.Bare);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("[API] Could not join the room: " + ex.Message);
            }

        }

        /// <summary>
        /// Adds or removes a contact from the roster
        /// </summary>
        /// <param name="sender"></param> the client
        /// <param name="e"></param>
        private void Client_OnRosterItem(object sender, Matrix.Xmpp.Roster.RosterEventArgs e)
        {
            var contact = Contacts.FirstOrDefault(c => c.Jid == e.RosterItem.Jid);

            if (e.RosterItem.Subscription != Matrix.Xmpp.Roster.Subscription.remove)
            {
                if (contact == null)
                {
                    // Sort contacts by name by default (to ease up their view)
                    // we iterate through the already existing list to find its position
                    // TODO: Possible it's faster to just load the list and sort it afterwards
                    var ContactName = e.RosterItem.Name ?? e.RosterItem.Jid;
                    int insertIndex = 0;
                    foreach (Contact existingContact in Contacts)
                    {
                        if (string.Compare(existingContact.Name, ContactName, StringComparison.OrdinalIgnoreCase) < 0)
                        {
                            insertIndex++;
                        }
                        else break;
                    }
                    Contacts.Insert(
                        insertIndex,
                        new Contact
                        {
                            Name = ContactName,
                            Jid = e.RosterItem.Jid,
                            Mobile = e.RosterItem.GetAttribute("mobile"),
                            MentionName = e.RosterItem.GetAttribute("mention_name")
                        });
                }
            }
            else
            {
                if (contact != null)
                    Contacts.Remove(contact);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        /*
         * <message type="groupchat" from="78728_die_hard@conf.hipchat.com/Nicholas Valbusa" mid="28295de1-e4b0-4b70-8f7f-bfe223d7981e" to="78728_820507@chat.hipchat.com/MatriX-WinRT" xmlns="jabber:client">
             <body>test</body>
             <delay xmlns="urn:xmpp:delay" stamp="2014-05-10T20:14:28Z" from_jid="78728_563904@chat.hipchat.com" />
           </message>
         */

        private void Client_OnMessage(object sender, MessageEventArgs e)
        {
            if (e.Message.Body == null) return;

            if (e.Message.Type == Matrix.Xmpp.MessageType.error)
            {
                return;
            }

            // Getting the correct 'From' attribute for the chat message
            Jid MessageFrom = e.Message.From;
            string rawDelay = e.Message.Delay.ToString();
            // TODO: See if the more complicated regex is actually needed
            //string delay_jid_regex = "from_jid=\"([^\"]+@[^\"]+)\"";
            string DelayJidPattern = "[^\"]+@[^\"]+";
            Regex r = new Regex(DelayJidPattern, RegexOptions.IgnoreCase);
            Match match = r.Match(rawDelay);

            if (match.Success)
            {
                MessageFrom.Parse(match.Value);
            }
            var msg = new HipChat.Model.ChatMessage
            {
                From = MessageFrom,
                Type = e.Message.Type,
                Body = e.Message.Body,
                Stamp = e.Message.Delay != null ? e.Message.Delay.Stamp : DateTime.Now
            };

            if (e.Message.Attribute("mid") != null)
            {
                msg.Mid = e.Message.Attribute("mid").Value;
            }

            // Show the message in the corresponding chat
            Chat chat = (from Chat c in Chats where c.Jid.Bare == e.Message.From.Bare select c).FirstOrDefault();

            if (chat != null)
            {
                if (msg.Mid != null)
                {
                    var exist = chat.Messages.FirstOrDefault(m => m.Mid == msg.Mid);
                    if (exist != null)
                    {
                        return;
                    }
                }

                //if (last != null)
                //{
                //    if (msg.Stamp.CompareTo(last.Stamp) < 0)
                //    {
                //        System.Diagnostics.Debug.WriteLine("Message skipped");
                //        return;
                //    }
                //}

                chat.Messages.Add(msg);
            }

            if (msg.IsOneToOne)
            {
                if (chat == null)
                {
                    Contact person = (from Contact c in Contacts where c.Jid == msg.From.Bare select c).FirstOrDefault();

                    if (person != null)
                    {
                        msg.From.Resource = person.Name;

                        chat = JoinOneToOneRoom(person);
                        chat.Messages.Add(msg);

                        AskJoinOneToOneRoom(msg);
                    }
                }

                if (chat != null && msg.IsVeryRecent())
                {
                    Notifications.DisplayToast(msg.FromName, msg.Body, "jid://" + chat.Jid.Bare);
                }
            }
            else if (chat != null)
            {
                (chat.Resource as Room).LastActive = msg.Stamp;

                // Room. Check if I was mentioned
                if (chat != null && msg.IsVeryRecent() && msg.Body.Contains(CurrentUser.MentionName))
                {
                    Notifications.DisplayToast(msg.FromName, msg.Body, "jid://" + chat.Jid.Bare);
                }
            }
        }

        private void AskJoinOneToOneRoom(ChatMessage msg)
        {
            JoinOneToOneRoom(msg.FromContact);
        }

        /// <summary>
        /// Updates the online presence of a user
        /// </summary>
        /// <param name="sender"></param> the client
        /// <param name="e"></param>
        void Client_OnPresence(object sender, PresenceEventArgs e)
        {
            string jid = e.Presence.From.Bare;
            var contact = Contacts.FirstOrDefault(c => c.Jid == jid);
            if (contact != null)
                contact.SetOnlineStateFromPresence(e.Presence);
        }

        /// <summary>
        /// Requests the rooms list asyncronously.
        /// They will be available using this.Rooms (ObservableCollection)
        /// http://jabber.org/protocol/disco#items
        /// </summary>
        public void GetRooms()
        {
            if (_isFetchingRooms) return;
            _isFetchingRooms = true;

            System.Diagnostics.Debug.WriteLine("[API] Requesting rooms...");
            disco.DiscoverItems("conf.hipchat.com");
        }

        /// <summary>
        /// Joins a public/private group chat and prepares its chatroom
        /// </summary>
        /// <param name="room"></param>
        public Chat JoinRoom(Room room, bool forceRejoin = false)
        {
            if (forceRejoin && room.chat != null)
            {
                System.Diagnostics.Debug.WriteLine("[CHATROOM] Rejoined: " + room.Jid.Bare);
                muc.EnterRoom(room.Jid, CurrentUser.Name, true);
            }
            else if (room.chat == null)
            {
                System.Diagnostics.Debug.WriteLine("[CHATROOM] Joined: " + room.Jid);

                room.chat = new Chat
                {
                    Resource = room,
                    Jid = room.Jid,
                    Name = room.Name
                };

                Chats.Add(room.chat);

                muc.EnterRoom(room.Jid, CurrentUser.Name);
            }

            return room.chat;
        }

        public void GetRoomMembers(Room room)
        {
            Iq x = new Iq();
            x.From = CurrentUser.Jid;
            x.To = room.Jid;
            x.Type = IqType.get;
            XNamespace ns = "http://jabber.org/protocol/disco#items";
            x.Query = new Matrix.Xml.XmppXElement(ns, "query");

            client.Send(x);

        }

        private void client_OnIq(object sender, IqEventArgs e)
        {
            if (e.Iq.From != null)
            {
                string jid = e.Iq.From.Bare;

                Room room = Rooms.FirstOrDefault(r => r.Jid.Bare == jid);

                if (room != null)
                {
                    XNamespace itemsNs = "http://jabber.org/protocol/disco#items";
                    if (e.Iq.Query != null)
                    {
                        XNamespace userNs = "http://jabber.org/protocol/muc#user";

                        foreach (XElement item in e.Iq.Descendants(userNs + "item"))
                        {
                            Contact contact = Contacts.FirstOrDefault(c => c.Jid == item.Attribute("jid").Value);
                            if (contact != null)
                            {
                                room.Members.Add(contact);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Prepares a one-to-one chatroom
        /// </summary>
        /// <param name="person"></param>
        public Chat JoinOneToOneRoom(Contact person)
        {
            if (person.chat == null)
            {
                System.Diagnostics.Debug.WriteLine("Chatroom added with person Jid " + person.Jid);

                person.chat = new Chat
                {
                    Resource = person,
                    Jid = person.Jid,
                    Name = person.Name
                };

                Chats.Add(person.chat);

                Iq x = new Iq();
                x.From = CurrentUser.Jid;
                x.To = person.Jid;
                x.Type = IqType.get;
                XNamespace ns = "http://hipchat.com/protocol/history";
                x.Query = new Matrix.Xml.XmppXElement(ns, "query");
                x.Query.SetAttribute("type", "chat");
                x.Query.SetAttribute("maxstanzas", "25");

                client.Send(x);
            }

            return person.chat;
        }

        public void SetChatTypingStatus(Chat chat, bool status)
        {
            var msg = new Matrix.Xmpp.Client.Message
            {
                Type = chat.IsRoom ? MessageType.groupchat : MessageType.chat,
                To = chat.Jid,
                Chatstate = status ? Matrix.Xmpp.Chatstates.Chatstate.composing : Matrix.Xmpp.Chatstates.Chatstate.active
            };
            client.Send(msg);
        }

        public void SetChatActiveStatus(Chat chat, bool active)
        {
            var msg = new Matrix.Xmpp.Client.Message
            {
                Type = chat.IsRoom ? MessageType.groupchat : MessageType.chat,
                To = chat.Jid,
                Chatstate = active ? Matrix.Xmpp.Chatstates.Chatstate.active : Matrix.Xmpp.Chatstates.Chatstate.inactive
            };
            client.Send(msg);
        }

        private void disco_OnRoomItemReceived(object sender, Matrix.Xmpp.Disco.DiscoIqEventArgs e)
        {
            foreach (Matrix.Xmpp.Disco.Item room in e.Items.GetItems())
            {
                var existingRoom = Rooms.FirstOrDefault(r => r.Jid.Bare == room.Jid.Bare);
                if (existingRoom != null)
                {
                    continue;
                }

                var last_active_ts = room.Descendants(nsRoom + "last_active").FirstOrDefault();
                DateTime last_active = last_active_ts != null ? dtDateTime.AddSeconds(Int32.Parse(room.Descendants(nsRoom + "last_active").FirstOrDefault().Value)).ToLocalTime() : DateTime.Now.AddYears(-1);

                Rooms.Add(
                    new Room
                    {
                        Name = room.Name ?? room.Jid,
                        Jid = room.Jid,
                        MembersCount = Int32.Parse(room.Descendants(nsRoom + "num_participants").FirstOrDefault().Value),
                        IsPrivate = room.Descendants(nsRoom + "privacy").FirstOrDefault().Value == "private",
                        Topic = room.Descendants(nsRoom + "topic").FirstOrDefault().Value ?? "",
                        LastActive = last_active
                    });
            }

            Account.Current.LoadOpenChats(API.Current);
            RejoinOpenChats();

            _isFetchingRooms = false;
        }

        public Chat GetChat(string bareJid, bool force = false)
        {
            Chat chat = API.Current.Chats.FirstOrDefault(s => s.Jid.Bare == bareJid);

            if (chat == null || force)
            {
                //Chat needs to be created
                if (bareJid.Contains("conf.hipchat.com"))
                {
                    Room room = Rooms.FirstOrDefault(r => r.Jid.Bare == bareJid);
                    if (room != null)
                    {
                        chat = JoinRoom(room, force);
                    }
                }
                else
                {
                    Contact person = Contacts.FirstOrDefault(c => c.Jid == bareJid);

                    if (person != null)
                    {
                        chat = JoinOneToOneRoom(person);
                    }
                }
            }

            return chat;
        }

        /// <summary>
        /// Leaves a group chat
        /// </summary>
        /// <param name="chat"></param>
        public void LeaveChatRoom(Chat chat)
        {
            if (chat.IsRoom)
            {
                muc.ExitRoom(chat.Jid.Bare, CurrentUser.Name);
            }

            Chats.Remove(chat);
            chat = null;
        }

        /// <summary>
        /// Sets the user online presence
        /// </summary>
        /// <param name="presence"></param>
        public void SetPresence(string presence)
        {
            System.Diagnostics.Debug.WriteLine("Setting account presence to " + presence);

            switch (presence)
            {
                case "dnd":
                    client.SendPresence(Show.dnd);
                    //Notifications.DisplayToast("Status changed to \"Do Not Disturb\"");
                    break;
                case "away":
                    client.SendPresence(Show.away);
                    //Notifications.DisplayToast("Status changed to \"Away\"");
                    break;
                case "offline":
                    client.SendUnavailablePresence("Offline");
                    break;
                default:
                    client.SendPresence(Show.chat);
                    //Notifications.DisplayToast("Status changed to \"Available\"");
                    break;
            }

            //Saves the new presence in the preferences
            Account.Current.OnlinePresence = presence;
        }

        public void SetOfflinePresence()
        {
            SetPresence("offline");
            CloseConnection();

            Contacts.Clear();
            Rooms.Clear();
            Chats.Clear();

            SetupClientInstance();
        }

        /// <summary>
        /// Sets the account presence using the value stored in the account preferences
        /// </summary>
        public void LoadAccountPresence()
        {
            string presence = Account.Current.OnlinePresence;

            if (presence != null && presence != "online")
            {
                switch (presence)
                {
                    case "away":
                        client.Show = Show.away;
                        break;
                    case "dnd":
                        client.Show = Show.dnd;
                        break;
                    default:
                        client.Show = Show.chat;
                        break;
                }
            }
        }

        #region License

        /// <summary>
        /// Sets the client license
        /// </summary>
        private void SetLicense()
        {
            Matrix.License.LicenseManager.SetLicense(HipChat.Config.Env.License());
        }

        #endregion
    }
}
