﻿using HipChat.Libs;
using HipChat.Model;
using Matrix;
using Matrix.Xmpp.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace HipChat
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PublicRoomPage : Page, IFileOpenPickerContinuable
    {
        private string bareJid;

        private Room room;
        private Contact person;
        public Chat chat;

        private bool attachmentFlyoutOpen;
        private StorageFile currentFile;

        private bool TemplateRendered = false;

        public PublicRoomPage()
        {
            this.InitializeComponent();

            this.messagesView.NavigationStarting += messagesView_NavigationStarting;
            this.attachmentFlyout.Opened += attachmentFlyout_Opened;
            this.attachmentFlyout.Closed += attachmentFlyout_Closed;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            // Deletes the bindings
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;

            API.Current.Disco.OnDiscoItemsResult -= Disco_OnDiscoItemsResult;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ShowProgress();

            bareJid = e.Parameter as string;

            API.Current.Reconnect(delegate(bool success)
            {
                if (success)
                {
                    chat = API.Current.GetChat(bareJid);

                    if (chat == null && API.Current.Chats.Count == 0)
                    {
                        // When resuming to this page from a suspended state, we have to get the rooms first
                        API.Current.Disco.OnDiscoItemsResult += Disco_OnDiscoItemsResult;
                    }
                    else
                    {
                        LoadChatRoom();
                    }
                }
                else
                {
                    BackToLobby_Click(null, null);
                }
            });

            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void ShowProgress(string text = "")
        {
            messagesView.Opacity = 0.6;
            ring.IsActive = true;
            loaderTxt.Text = text;
            loader.Visibility = Visibility.Visible;
        }

        private void HideProgress()
        {
            loader.Visibility = Visibility.Collapsed;
            messagesView.Opacity = 1.0;
            ring.IsActive = false;
        }

        private void Disco_OnDiscoItemsResult(object sender, Matrix.Xmpp.Disco.DiscoIqEventArgs e)
        {
            chat = API.Current.GetChat(bareJid);
            LoadChatRoom();
        }

        private void LoadChatRoom()
        {
            if (chat == null)
            {
                Frame.Navigate(typeof(MainPage));
                return;
            }

            if (chat.IsRoom)
            {
                room = chat.Resource as Room;
                roomName.Text = room.Name;
                UpdateRoomDescription();

                membersList.ItemsSource = room.Members;
            }
            else
            {
                person = chat.Resource as Contact;
                roomName.Text = person.Name;
                var converter = new OnlineStateDescriptionConverter();
                roomDescription.Text = converter.Convert(person.ExtendedOnlineState, null, null, null).ToString();

                leaveRoomBtn.Label = "leave chat";

                commandBar.PrimaryCommands.Remove(membersButton);
            }

            LoadHTMLTemplate();

            //chat.Active = true;
        }

        private void UpdateRoomDescription()
        {
            if (room != null) roomDescription.Text = room.DescriptionActivity;
        }

        private void Messages_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (ChatMessage message in e.NewItems)
                {
                    RenderMessage(message);
                }

                if (!sendButton.IsEnabled) sendButton.IsEnabled = true;
            }
        }

        private void LoadHTMLTemplate()
        {
            if (TemplateRendered) return;
            TemplateRendered = true;

            messagesView.Opacity = 0.35;
            messagesView.DOMContentLoaded += messagesView_DOMContentLoaded;
            messagesView.Source = new System.Uri("ms-appx-web:///Assets/HTML/messages-list.html");
        }

        private async void messagesView_DOMContentLoaded(WebView sender, WebViewDOMContentLoadedEventArgs args)
        {
            await messagesView.InvokeScriptAsync("setUser", new string[] { API.Current.CurrentUser.Name, API.Current.CurrentUser.MentionName });

            int toDisplay = 25;
            int count = chat.Messages.Count;

            if (count > toDisplay)
            {
                // Displays just few messages
                RenderMessages(chat.Messages.Skip(count - toDisplay).Take(toDisplay));
            }
            else
            {
                // Displays all the messages
                RenderMessages(chat.Messages);
            }

            // Adds the bindings
            HideProgress();
            chat.Messages.CollectionChanged += Messages_CollectionChanged;
        }

        private void RenderMessages(IEnumerable<ChatMessage> messages)
        {
            foreach (ChatMessage message in messages)
            {
                RenderMessage(message);
            }
        }

        private async void RenderMessage(ChatMessage message)
        {
            if (message.Body == null) return;

            try
            {
                var args = new string[3];
                args[0] = message.FromName;
                args[1] = message.Body;
                args[2] = message.StampTimeOnly;

                await messagesView.InvokeScriptAsync("displayMessage", args);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Cannot render message: " + ex.Message);
            }
        }

        private async void messagesView_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
            if (null != args.Uri && (args.Uri.OriginalString.Contains("http://") || args.Uri.OriginalString.Contains("https://") ) )
            {
                args.Cancel = true;
                await Launcher.LaunchUriAsync(args.Uri);
            }
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            if (!this.Frame.CanGoBack)
            {
                e.Handled = true;
                this.Frame.Navigate(typeof(MainPage));
            }
        }

        private void messageTxt_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            int length = messageTxt.Text.Length;

            if (length >= 1)
            {
                chat.IsTyping = true;
            } else if (length == 0)
            {
                chat.IsTyping = false;
            }

            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                // For the time being, we allow the return key
            }
        }

        /// <summary>
        /// Sends a message
        /// </summary>
        private void SendMessage()
        {
            var msg = messageTxt.Text;

            messageTxt.Text = "";

            sendButton.IsEnabled = false;

            chat.SendMessage(msg);

            UpdateRoomDescription();
        }

        private void AttachButton_Click(object sender, RoutedEventArgs e)
        {
            Files.SelectFile("PublicRoomAttachFile");
        }

        public void ContinueFileOpenPicker(FileOpenPickerContinuationEventArgs args)
        {
            if ((args.ContinuationData["Operation"] as string) == "PublicRoomAttachFile" &&
                args.Files.Count > 0)
            {
                ShowProgress("Reading file");

                API.Current.Reconnect(async delegate(bool success)
                {
                    HideProgress();

                    StorageFile file = args.Files[0];

                    if (file != null)
                    {
                        currentFile = file;

                        attachmentMessage.Text = "";

                        // Force rejoin
                        chat = API.Current.GetChat(chat.Jid, true);

                        if (currentFile.ContentType.IndexOf("image") == 0)
                        {
                            attachmentImage.Source = await GetImageAsync(currentFile);
                            attachmentImage.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            attachmentImage.Visibility = Visibility.Collapsed;
                            attachmentTopDescription.Text = "Please type a description (optional) and tap the upload button.\r\nFile: " + currentFile.Name;
                        }

                        attachmentFlyout.ShowAt(this.LayoutRoot);
                    }
                });
            }
        }

        private void BackToLobby_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }

        private async Task<BitmapImage> GetImageAsync(StorageFile storageFile)
        {
            BitmapImage bitmapImage = new BitmapImage();
            FileRandomAccessStream stream = (FileRandomAccessStream)await storageFile.OpenAsync(FileAccessMode.Read);
            bitmapImage.SetSource(stream);
            return bitmapImage;
        }

        private void LeaveRoom_Click(object sender, RoutedEventArgs e)
        {
            if (chat != null) chat.Leave();
            BackToLobby_Click(sender, e);
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            if (attachmentFlyoutOpen)
            {
                ConfirmAttachButton_Click(null, null);
                return;
            }

            if (messageTxt.Text == "")
            {
                messageTxt.Focus(FocusState.Keyboard);
                return;
            }

            SendMessage();
        }

        private void Member_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Contact person = (sender as Grid).DataContext as Contact;

            var toAdd = String.Format("@{0} ", person.MentionName);

            if (messageTxt.Text.Length == 0) messageTxt.Text = toAdd;
            else messageTxt.Text += " " + toAdd;

            membersFlyout.Hide();
            messageTxt.Focus(FocusState.Keyboard);
            messageTxt.Select(messageTxt.Text.Length - 1, 0);
        }

        private async void ConfirmAttachButton_Click(object sender, RoutedEventArgs e)
        {
            attachButton.IsEnabled = false;
            ShowProgress("Uploading file");

            attachmentFlyout.Hide();

            Exception exc = null;

            try
            {
                var path = await Files.UploadFile(currentFile);
                if (path != null)
                {
                    if (attachmentMessage.Text.Length > 0)
                    {
                        chat.SendMessage(attachmentMessage.Text.Trim() + " " + path);
                    }
                    else
                    {
                        chat.SendMessage(path);
                    }
                }
            }
            catch (Exception ex)
            {
                exc = ex;
            }

            attachButton.IsEnabled = true;
            HideProgress();

            if (exc != null)
            {
                var dlg = new MessageDialog(exc.Message, "Upload error");
                dlg.Commands.Add(new UICommand("Ok"));
                await dlg.ShowAsync();
            }
        }



        private void attachmentFlyout_Closed(object sender, object e)
        {
            attachmentFlyoutOpen = false;
        }

        private void attachmentFlyout_Opened(object sender, object e)
        {
            attachmentFlyoutOpen = true;
        }
    }
}
